package br.com.itau.pedidos.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.pedidos.models.Pedido;
import br.com.itau.pedidos.repositories.PedidoRepository;

@Service
public class PedidoService {

	@Autowired
	PedidoRepository pedidoRepository;

	public Pedido salvar(Pedido pedido) {
		return pedidoRepository.save(pedido);
	}
}
