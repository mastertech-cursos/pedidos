package br.com.itau.pedidos.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import br.com.itau.pedidos.models.Pagamento;

@FeignClient(name = "pagamento", fallback = PagamentoClientFallback.class)
public interface PagamentoClient {

	@PostMapping
	@HystrixCommand
	Pagamento verifica(@RequestBody Pagamento pagamento);
}
