package br.com.itau.pedidos.controllers;

import static org.springframework.http.HttpStatus.NO_CONTENT;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.pedidos.models.Curso;
import br.com.itau.pedidos.models.Pedido;
import br.com.itau.pedidos.services.CursoService;
import br.com.itau.pedidos.services.PagamentoService;
import br.com.itau.pedidos.services.PedidoService;

@RestController
public class PedidosController {

	@Autowired
	CursoService cursoService;

	@Autowired
	PagamentoService pagamentoService;

	@Autowired
	PedidoService pedidoService;

	@PostMapping
	public ResponseEntity<Pedido> criarPedido(@RequestBody Pedido pedido) {

		Optional<Curso> curso = cursoService.busca(pedido.getNomeCurso());
		if (!curso.isPresent()) {
			return ResponseEntity
					.status(NO_CONTENT)
					.build();
		}

		boolean statusPagamento = pagamentoService.verificaStatus(curso.get());
		pedido.setStatusPagamento(statusPagamento);
		pedido.setTimestamp(LocalDateTime.now());

		Pedido pedidoSalvo = pedidoService.salvar(pedido);
		return ResponseEntity.ok(pedidoSalvo);
	}
}
